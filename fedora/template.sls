/etc/pki/rpm-gpg/RPM-GPG-KEY-rpmfusion-free-fedora-37:
  file.managed:
    - source: salt://fedora/RPM-GPG-KEY-rpmfusion-free-fedora-37

rpmfusion-free:
  pkgrepo.managed:
    - humanname: RPM Fusion for Fedora $releasever - Free
    - mirrorlist: https://mirrors.rpmfusion.org/metalink?repo=free-fedora-$releasever&arch=$basearch
    - gpgcheck: 1
    - gpgkey: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-rpmfusion-free-fedora-$releasever

rpmfusion-free-updates:
  pkgrepo.managed:
    - humanname: RPM Fusion for Fedora $releasever - Free - Updates
    - mirrorlist: https://mirrors.rpmfusion.org/metalink?repo=free-fedora-updates-released-$releasever&arch=$basearch
    - gpgcheck: 1
    - gpgkey: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-rpmfusion-free-fedora-$releasever

install-contrib-repo:
  pkg.installed:
    - pkgs:
      - qubes-repo-contrib

install-packages:
  pkg.installed:
    - pkgs:
      - qubes-split-browser
      - chromaprint-tools
      - ffmpeg
      - fuse-sshfs
      - less
      - ristretto
      - man-db
      - mcomix3
      - mediainfo
      - mplus-1m-fonts
      - mpv
      - nano
      - ncdu
      - picard
      - pulseaudio-qubes
      - qubes-core-agent-networking
      - qubes-core-agent-thunar
      - quodlibet
      - Thunar
      - firefox
      - xfce4-terminal
      - whois
      - mozilla-ublock-origin
      - mozilla-noscript
      - thunderbird
      - python3-pip
      - fuse
      - openssh-server
      - openssh-askpass
      - keepassxc
      - netcat
      - bc
      - engrampa
      - git
      - bind-utils
      - podman
      - ansible
      - tor
      - python3-netaddr
      - openssl
      - rsync
      - mousepad
      - iputils
      - syncthing
      - bash-completion
      - wireguard-tools
      - xfce4-notifyd
      - sssd-client
      - dbus-x11
      - notification-daemon

/etc/qubes-rpc/qubes.SshAgent:
  file.managed:
    - mode: '0755'
    - contents: |
        #!/bin/sh

        exec socat - "UNIX-CONNECT:$SSH_AUTH_SOCK"

# systemd unit files from https://ryansquared.pub/posts/exploring-qubes-rpc
# thanks!

/etc/systemd/user/ssh-agent@.service:
  file.managed:
    - contents: |
        [Unit]
        Description=Forward connections to an SSH agent to a remote Qube

        [Service]
        ExecStart=qrexec-client-vm ssh-vault-legacy qubes.SshAgent
        StandardInput=socket
        StandardError=journal

/etc/systemd/user/ssh-agent.socket:
  file.managed:
    - contents: |
        [Unit]
        Description=Forward connections to an SSH agent to a remote Qube

        [Socket]
        ListenStream=%t/ssh/S.ssh-agent
        SocketMode=0600
        DirectoryMode=0700
        Accept=true

        [Install]
        WantedBy=sockets.target

/usr/lib64/firefox/browser/defaults/preferences/90-custom.js:
  file.managed:
    - contents: |
        pref("browser.search.suggest.enabled", false);
        pref("signon.rememberSignons", false);

        // "Show alerts about passwords for breached websites"
        pref("signon.management.page.breach-alerts.enabled", false);

        pref("browser.startup.homepage", "about:blank");

        // don't confirm window close when multiple tabs are open
        pref("browser.tabs.warnOnClose", false);

        // don't show version specific landing page
        pref("browser.startup.homepage_override.mstone", "ignore");

        // Settings -> Privacy & Security -> Allow Firefox to send technical and interaction data to Mozilla
        pref("datareporting.healthreport.uploadEnabled", false);

        // open about:blank on first run instead of mozilla's privacy notice
        pref("datareporting.policy.firstRunURL", "about:blank");

        pref("dom.security.https_only_mode", true);
        pref("browser.contentblocking.category", "custom");
        pref("privacy.trackingprotection.enabled", true);
        pref("privacy.trackingprotection.socialtracking.enabled", true);
        pref("datareporting.policy.dataSubmissionEnabled", false);
        pref("toolkit.telemetry.coverage.opt-out", true);
        pref("toolkit.coverage.opt-out", true);
        pref("extensions.pocket.enabled", false);

/usr/lib64/firefox/defaults/pref/autoconfig.js:
  file.managed:
    - contents: |
        pref("general.config.filename", "firefox.cfg");
        pref("general.config.obscure_value", 0);
        pref("general.config.sandbox_enabled", false);

/usr/lib64/firefox/firefox.cfg:
  file.managed:
    - contents: |
        //
        (async () => {
          "use strict";

          const { Services } = ChromeUtils.import("resource://gre/modules/Services.jsm");

          await Services.search.getDefault();

          let engine = Services.search.getEngineByName("DuckDuckGo");
          Services.search.setDefault(
            engine,
            Ci.nsISearchService.CHANGE_REASON_ENTERPRISE
          );
        })();

