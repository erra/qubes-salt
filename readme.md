a fedora template with most things i need. minimal template used as a base.

- enable rpmfusion repos
- install qubes contrib repo (https://www.qubes-os.org/doc/installing-contributed-packages/)
- install packages
- install split ssh-agent
- configure firefox settings
- set ddg as default search engine

### ssh client qube configuration

	$ systemctl --user enable --now ssh-agent.socket
	$ cat .profile 
	#!/bin/sh
	
	export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh/S.ssh-agent"

### useful related links

- https://rpmfusion.org/keys
- https://www.qubes-os.org/doc/how-to-copy-from-dom0/#copying-to-dom0
- https://github.com/unman/shaker
- https://ryansquared.pub/posts/exploring-qubes-rpc
- https://searchfox.org/
- https://github.com/rustybird/qubes-app-split-browser
- https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig
